# README #

This is a standalone REST API server. It uses an in-memory DB for persistence, and exposes a single resource endpoint that represents products that are for sale.

### Environment dependencies ###
- Gradle 3.5 [installation instructions](https://gradle.org/install)
- JDK 1.8 [installation instructions](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)

### Get started ###

1. Start the REST server:
 
    ```
    ./gradle bootRun
    ```

2. Seed the mock development data:
 
    ```
    ./seed-items.sh
    ```

3. The server will be available at [http://localhost:8081](http://localhost:8081)