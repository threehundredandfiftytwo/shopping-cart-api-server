package cart;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RepositoryRestResource(collectionResourceRel = "saleable-items", path = "saleable-items")
public interface SaleableItemRepository extends PagingAndSortingRepository<SaleableItem, Long> {}
